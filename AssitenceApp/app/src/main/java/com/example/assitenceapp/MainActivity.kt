package com.example.assitenceapp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.util.Log
import android.bluetooth.BluetoothAdapter
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import java.text.SimpleDateFormat
import java.util.*
import org.jetbrains.anko.toast
import android.R.*
import android.content.Intent




//import android.content.Intent
//import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {
    /*
    var m_bluetoothAdapter: BluetoothAdapter? = null
    val REQUEST_ENABLE_BLUETOOTH = 1
    */

    lateinit var mBluetoothAdapter: BluetoothAdapter

    companion object {
        val EXTRA_ADRESS: String = "Device_address"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun turnOnBlueTooth(view: View){
        try{
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

            if (!mBluetoothAdapter.isDiscovering()) {
                val intent = Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE)
                startActivityForResult(intent, 1)
            } else {
                toast("El bluetooth ya esta en modo discovery")
            }
        }catch (e:Exception){
            e.printStackTrace()
        }
    }

    fun verificate(view: View){

        val requestQ = Volley.newRequestQueue(this)



        var serviceURL = "https://firestore.googleapis.com/v1/projects/attendancelistapp/databases/(default)/documents/attendance/";
        var date = Date();
        val fecha = SimpleDateFormat("yyyy-MM-dd").format(Date());
        val numeros = fecha.split("-");
        val anio = numeros[0];
        val mes = numeros[1].toInt()-1;
        val dia = numeros[2];

        serviceURL = serviceURL + dia + "-" + mes.toString() + "-" + anio + "/students/201630956";

        Log.i("Url", serviceURL);
        val stringRequest = StringRequest(serviceURL, Response.Listener<String>{response ->

            toast("La asistencia de hoy fue guardada con exito");

        }, Response.ErrorListener{error ->
            toast("No hay registro");
        })

        requestQ.add(stringRequest);
    }
}
